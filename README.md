# Paddle-RLBooks

Welcome to Paddle-RLBooks which is a reinforcement learning code study guide based on pure PaddlePaddle.

## Show
![](./material/FlappyBird.gif)

## Codes
- [Policy Iteration](./policy_iteration)
- [Value Iteration](./value_iteration)
- [Sarsa](./sarsa)
- [Q-learning](./qlearning)
- [DQN](./dqn)
- [Policy Gradient](./policy_gradient)
- [Actor-Critic](./actor_critic)

## Contact us
Email : [agentmaker@163.com]()